# WeDooGift's Technical Test - Laure Bompaire

## Introduction

This challenge is about creating a program so that a Company can create and distribute Vouchers to ist Users.

The code is divided in Repository/Service/Controller and the Controller is exposing a REST API. To get the correct output, call the REST API on the address [localhost:8080/run](http://localhost:8080/run).

If you want to see step by step computation you can:

* Check the input file using [localhost:8080/readInputFile](http://localhost:8080/readInputFile)
* Load data using [localhost:8080/loadData](http://localhost:8080/loadData)
* Once the data are loaded, you can distribute voucher for a specific DistributionCampaign using [localhost:8080/distributeVoucher?distributionCampaignId=1](http://localhost:8080/distributeVoucher?distributionCampaignId=1) (for DistributionCampaign 1)
* Check a specific user balance using [localhost:8080/getBalance?userId=1](http://localhost:8080/getBalance?userId=1) (for User 1)

### Credentials
Two different profiles are implemented:
* userName:**user** password:**123456** who has access to readInputFile and getBalance
* userName:**admin** password:**123456** who has access to everything.


## Technologies

The project was developed in Java using SpringBoot and was compiled with Gradle. The unit tests were written in Junit.

* Java v16.0.1
* SpringBoot v2.4.1
* SpringSecurity v5.4.2
* Junit v5.7.0
* Gradle v7.0

## Launch

To launch the application execute the next command from the project root directory.

`./gradlew bootRun`

