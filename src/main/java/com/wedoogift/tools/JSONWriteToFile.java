package com.wedoogift.tools;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.DTO.ValuesDTO;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

@Component
public class JSONWriteToFile {

    public void writeValuesDTOToJSONFile(ValuesDTO valuesDTO, String fileName){
        FileWriter file;
        try {
            file = new FileWriter(fileName);
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                file.write(objectMapper.writeValueAsString(valuesDTO));
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                file.flush();
                file.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
