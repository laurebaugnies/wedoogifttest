package com.wedoogift.tools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.DTO.ValuesDTO;
import com.wedoogift.entity.Company;
import com.wedoogift.entity.Distribution;
import com.wedoogift.entity.User;
import com.wedoogift.service.VoucherService;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class JSONReadFromFile {


    @Autowired
    private VoucherService voucherService;


    public ValuesDTO getValuesDTOFromJSONFile(String fileName){
        JSONObject jsonObject = getJSONObjectFromFile(fileName);
        return getData(jsonObject);
    }

    public JSONObject getJSONObjectFromFile(String fileName) {
       JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(fileName));

           return (JSONObject) obj;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ValuesDTO getData(JSONObject jsonObject){
        ValuesDTO valuesDTO = new ValuesDTO();

        valuesDTO.getCompanies().addAll(getListForEntity(jsonObject, "companies", Company.class));

        JSONArray jsonUserList = (JSONArray) jsonObject.get("users");
        jsonUserList.forEach(json -> {
            User user = new User();
            user.setId(((Integer) ((JSONObject) json).get("id")).longValue());
            JSONArray balance = (JSONArray) ((JSONObject) json).get("balance");
            balance.forEach(jsonNested ->
                    voucherService.addVoucherToUser(user, ((Integer) ((JSONObject) jsonNested).get("amount")).longValue(),
                            (int) ((JSONObject) jsonNested).get("wallet_id")));
            valuesDTO.getUsers().add(user);
        });

        valuesDTO.getDistributionList().addAll(getListForEntity(jsonObject, "distributions", Distribution.class));

        return valuesDTO;
    }

    public <T> List<T> getListForEntity(JSONObject jsonObject, String fieldName, Class<T> className){
        JSONArray jsonList = (JSONArray) jsonObject.get(fieldName);
        List<T> objectList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        jsonList.forEach(json -> {
            try {
                objectList.add(objectMapper.readValue(((JSONObject) json).toJSONString(), className));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        return objectList;
    }
}
