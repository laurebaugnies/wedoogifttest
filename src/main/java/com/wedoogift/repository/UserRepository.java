package com.wedoogift.repository;

import com.wedoogift.entity.User;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Workaround of a real repository class
 */
@Repository
public class UserRepository {

    Map<Long, User> fakeDb = new HashMap<>();

    public User findById(@NonNull final long userId){
        return fakeDb.get(userId);
    }

    public User create(@NonNull final User user){
        fakeDb.put(user.getId(), user);
        return user;
    }
}