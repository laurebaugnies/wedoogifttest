package com.wedoogift.repository;

import com.wedoogift.entity.DistributionCampaign;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Workaround of a real repository class
 */
@Repository
public class DistributionCampaignRepository {

    Map<Long, DistributionCampaign> fakeDb = new HashMap<>();

    private long idCounter = 0;

    public DistributionCampaign findById(@NonNull final long distributionCampaignId){
        return fakeDb.get(distributionCampaignId);
    }

    public DistributionCampaign create(@NonNull final DistributionCampaign distributionCampaign){
        idCounter++;
        distributionCampaign.setId(idCounter);
        fakeDb.put(idCounter, distributionCampaign);
        return distributionCampaign;
    }
}
