package com.wedoogift.controller;

import com.wedoogift.service.DistributionCampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@PreAuthorize("hasRole('ROLE_ADMIN')")
@RestController
public class DistributionCampaignController {

    @Autowired
    private DistributionCampaignService distributionCampaignService;

    @GetMapping("/distributeVoucher")
    public boolean distributeVoucherForCompany(@RequestParam(value="distributionCampaignId") final long distributionCampaignId) {
        return distributionCampaignService.distributeVoucher(distributionCampaignId);
    }
}
