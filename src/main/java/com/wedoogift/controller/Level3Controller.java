package com.wedoogift.controller;

import com.wedoogift.DTO.ValuesDTO;
import com.wedoogift.entity.DistributionCampaign;
import com.wedoogift.service.Level3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Level3Controller {

    @Autowired
    private Level3Service service;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/run")
    public ValuesDTO run() {
        return service.run();
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping("/readInputFile")
    public ValuesDTO readInputFile() {
        return service.readInputFile();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/loadData")
    public List<DistributionCampaign> loadData() {
        return service.loadData();
    }
}
