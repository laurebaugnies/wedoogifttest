package com.wedoogift.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.wedoogift.entity.Company;
import com.wedoogift.entity.Distribution;
import com.wedoogift.entity.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@JsonPropertyOrder({ "companies", "users", "distributions" })
@JsonIgnoreProperties(value={ "distributionList" })
public class ValuesDTO {

    private List<Company> companies = new ArrayList<>();
    private List<User> users = new ArrayList<>();
    private final List<Distribution> distributions = new ArrayList<>();

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Distribution> getDistributionList() {
        return distributions;
    }

    //===================For JSON purpose==============================

    public List<Distribution> getDistributions() {
        return distributions
                .stream()
                .sorted(Comparator.comparingLong(Distribution::getId))
                .collect(Collectors.toList());
    }
}
