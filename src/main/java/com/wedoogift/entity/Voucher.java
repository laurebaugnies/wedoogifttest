package com.wedoogift.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDate;

@JsonIgnoreProperties(value={ "startDate", "endDate", "voucherType", "currentlyValid" })
public abstract class Voucher {

    private long amount;

    private LocalDate startDate;

    private LocalDate endDate;

    private VoucherType voucherType;

    public Voucher(long amount, LocalDate startDate, VoucherType voucherType) {
        this.amount = amount;
        this.startDate = startDate;
        endDate = calculateEndDate(startDate);
        this.voucherType = voucherType;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public boolean isCurrentlyValid(){
        return endDate.isAfter(LocalDate.now());
    }

    public abstract LocalDate calculateEndDate(LocalDate startDate);

    public int getWallet_id(){
        return voucherType.getId();
    }

    public void setWallet_id(int wallet_id){
        if(wallet_id == VoucherType.GIFT.getId()){
            voucherType = VoucherType.GIFT;
        } else if(wallet_id == VoucherType.FOOD.getId()){
            voucherType = VoucherType.FOOD;
        } else {
            voucherType = VoucherType.UNRECOGNIZED;
        }
    }
}
