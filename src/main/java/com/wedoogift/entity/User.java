package com.wedoogift.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(value={ "account" })
public class User {
    
    private long id;

    private Account account = new Account();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }


    //===================For JSON purpose==============================

    public List<Voucher> getBalance() {
        return getAccount().getVoucherList();
    }
}
