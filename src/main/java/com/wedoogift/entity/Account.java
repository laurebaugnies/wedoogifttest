package com.wedoogift.entity;

import java.util.ArrayList;
import java.util.List;

public class Account {

    private final List<Voucher> voucherList = new ArrayList<>();

    public List<Voucher> getVoucherList() {
        return voucherList;
    }

    public long getBalance(){
        return voucherList.stream()
                .filter(Voucher::isCurrentlyValid)
                .mapToLong(Voucher::getAmount)
                .sum();
    }
}
