package com.wedoogift.entity;

import java.util.ArrayList;
import java.util.List;

public class DistributionCampaign {

    private long id;

    private Company company = new Company();

    private final List<Distribution> distributionList = new ArrayList<>();

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Distribution> getDistributionList() {
        return distributionList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isBalanceSufficient(){
        return getCompany().getBalance() >=
                getDistributionList().stream()
                .mapToLong(Distribution::getAmount)
                .sum();
    }
}
