package com.wedoogift.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "id", "wallet_id", "amount", "start_date", "end_date", "company_id", "user_id" })
@JsonIgnoreProperties(value={ "voucher", "company", "user" })
public class Distribution {
    private long id;

    private Voucher voucher;

    private Company company;

    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAmount() {
        return voucher.getAmount();
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Voucher getVoucher() {
        return voucher;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }


    //===================For JSON purpose==============================

    //Workaround to format date in json output since JSONFormat annotation is not working
    public String getStart_date() {
        return voucher.getStartDate().toString();
    }

    //Workaround to format date in json output since JSONFormat annotation is not working
    public String getEnd_date() {
        return voucher.getEndDate().toString();
    }

    public long getCompany_Id(){
        return getCompany().getId();
    }

    public long getUser_Id(){
        return getUser().getId();
    }

    public long getWallet_Id(){
        return getVoucher().getWallet_id();
    }
}
