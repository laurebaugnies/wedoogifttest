package com.wedoogift.entity;

import java.time.LocalDate;

public class MealVoucher extends Voucher {

    public MealVoucher(long amount, LocalDate startDate) {
        super(amount, startDate, VoucherType.FOOD);
    }

    public final LocalDate calculateEndDate(LocalDate startDate){
        LocalDate firstOfFebruary = LocalDate.of(startDate.plusYears(1).getYear(),2,1);
        return firstOfFebruary.plusDays(firstOfFebruary.lengthOfMonth() - 1);
    }
}
