package com.wedoogift.entity;

import java.time.LocalDate;

public class GiftVoucher extends Voucher{

    private static final int GIFT_VOUCHER_LIFESPAN_IN_DAYS = 364;

    public GiftVoucher(long amount, LocalDate startDate) {
        super(amount, startDate, VoucherType.GIFT);
    }

    public final LocalDate calculateEndDate(LocalDate startDate){
        return startDate.plusDays(GIFT_VOUCHER_LIFESPAN_IN_DAYS);
    }
}
