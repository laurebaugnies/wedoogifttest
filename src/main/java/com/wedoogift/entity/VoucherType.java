package com.wedoogift.entity;

public enum VoucherType {
    GIFT(1), FOOD(2), UNRECOGNIZED(0);

    private final int id;

    VoucherType(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
