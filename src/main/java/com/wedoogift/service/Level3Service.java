package com.wedoogift.service;

import com.wedoogift.DTO.ValuesDTO;
import com.wedoogift.tools.JSONReadFromFile;
import com.wedoogift.entity.VoucherType;
import com.wedoogift.entity.DistributionCampaign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class Level3Service {

    @Autowired
    private JSONReadFromFile jsonReadFromFile;

    @Autowired
    private DistributionCampaignService distributionCampaignService;

    @Autowired
    private UserService userService;

    public ValuesDTO run() {
        ValuesDTO valuesDTO = readInputFile();
        List<DistributionCampaign> distributionCampaignList = prepareLevelData(valuesDTO);
        distributionCampaignList.forEach(
                distributionCampaign -> distributionCampaignService.distributeVoucher(distributionCampaign)
        );
        return valuesDTO;
    }

    public ValuesDTO readInputFile() {
        return jsonReadFromFile.getValuesDTOFromJSONFile("src/main/resources/Level2/data/input.json");
    }

    public List<DistributionCampaign> loadData() {
        return prepareLevelData(readInputFile());
    }


    private List<DistributionCampaign> prepareLevelData(ValuesDTO valuesDTO){
        valuesDTO.getUsers().forEach(userService::create);


        List<DistributionCampaign> distributionCampaignList = new ArrayList<>();

        DistributionCampaign distributionCampaignWeDooGift = new DistributionCampaign();
        distributionCampaignWeDooGift.setCompany(valuesDTO.getCompanies().get(0));
        distributionCampaignService.addDistributionToDistributionCampaign(
                distributionCampaignWeDooGift, 1, 50, LocalDate.of(2020, 9, 16), valuesDTO.getUsers().get(0));
        distributionCampaignService.addDistributionToDistributionCampaign(
                distributionCampaignWeDooGift, 2, 100, LocalDate.of(2020, 8, 1), valuesDTO.getUsers().get(1));
        distributionCampaignService.addDistributionToDistributionCampaign(
                distributionCampaignWeDooGift, 4, 250, LocalDate.of(2020, 5, 1), valuesDTO.getUsers().get(0), VoucherType.FOOD);
        valuesDTO.getDistributionList().addAll(distributionCampaignWeDooGift.getDistributionList());
        distributionCampaignList.add(distributionCampaignService.create(distributionCampaignWeDooGift));

        DistributionCampaign distributionCampaignWeDooFood = new DistributionCampaign();
        distributionCampaignWeDooFood.setCompany(valuesDTO.getCompanies().get(1));
        distributionCampaignService.addDistributionToDistributionCampaign(
                distributionCampaignWeDooFood, 3, 1000, LocalDate.of(2020, 5, 1), valuesDTO.getUsers().get(2));
        valuesDTO.getDistributionList().addAll(distributionCampaignWeDooFood.getDistributionList());
        distributionCampaignList.add(distributionCampaignService.create(distributionCampaignWeDooFood));

        return distributionCampaignList;
    }
}
