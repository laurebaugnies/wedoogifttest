package com.wedoogift.service;

import com.wedoogift.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DistributionService {

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private UserService userService;

    @Autowired
    private CompanyService companyService;

    public Distribution createGiftVoucherDistribution(@NonNull long id,
                                                      @NonNull long amount,
                                                      @NonNull LocalDate startDate,
                                                      @NonNull Company company,
                                                      @NonNull User user){
        Distribution distribution = createDistribution(id, company, user);
        distribution.setVoucher(voucherService.createGiftVoucher(amount, startDate));
        return distribution;
    }

    public Distribution createMealVoucherDistribution(@NonNull long id,
                                                      @NonNull long amount,
                                                      @NonNull LocalDate startDate,
                                                      @NonNull Company company,
                                                      @NonNull User user){
        Distribution distribution = createDistribution(id, company, user);
        distribution.setVoucher(voucherService.createMealVoucher(amount, startDate));
        return distribution;
    }

    private Distribution createDistribution(@NonNull long id,
                                            @NonNull Company company,
                                            @NonNull User user){
        Distribution distribution = new Distribution();
        distribution.setId(id);
        distribution.setCompany(company);
        distribution.setUser(user);
        return distribution;
    }

    public void distributeVoucher(Distribution distribution){
        userService.addVoucherToUser(distribution.getUser(), distribution.getVoucher());
        companyService.adjustBalanceAfterDistribution(distribution);
    }
}
