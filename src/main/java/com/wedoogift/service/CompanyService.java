package com.wedoogift.service;

import com.wedoogift.entity.Company;
import com.wedoogift.entity.Distribution;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    public void adjustBalanceAfterDistribution(@NonNull Distribution distribution){
        Company company = distribution.getCompany();
        company.setBalance(company.getBalance() - distribution.getAmount());
    }
}
