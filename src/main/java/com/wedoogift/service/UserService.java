package com.wedoogift.service;

import com.wedoogift.entity.User;
import com.wedoogift.entity.Voucher;
import com.wedoogift.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class UserService {

    @Autowired
    private AccountService accountService;

    @Autowired
    private UserRepository userRepository;

    public void addVoucherToUser(@NonNull User user, @NonNull Voucher voucher){
        accountService.addVoucherToAccount(user.getAccount(), voucher);
    }

    public long getUserBalance(@NonNull final long userId){
        AtomicLong balance = new AtomicLong();
        findById(userId).ifPresent(user -> balance.set(user.getAccount().getBalance()));
        return balance.get();
    }

    public User create(@NonNull final User user){
        return userRepository.create(user);
    }

    public Optional<User> findById(@NonNull final long userId){
        return Optional.ofNullable(userRepository.findById(userId));
    }
}
