package com.wedoogift.service;

import com.wedoogift.entity.VoucherType;
import com.wedoogift.entity.DistributionCampaign;
import com.wedoogift.entity.User;
import com.wedoogift.repository.DistributionCampaignRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class DistributionCampaignService {

    @Autowired
    private DistributionService distributionService;

    @Autowired
    private DistributionCampaignRepository distributionCampaignRepository;

    public boolean distributeVoucher(final long distributionCampaignId){
        Optional<DistributionCampaign> distributionCampaign = findById(distributionCampaignId);
        if(distributionCampaign.isPresent()){
            distributeVoucher(distributionCampaign.get());
            return true;
        }
        return false;
    }

    public void distributeVoucher(@NonNull DistributionCampaign distributionCampaign){
        if(distributionCampaign.isBalanceSufficient()){
            distributionCampaign.getDistributionList().forEach(distribution -> distributionService.distributeVoucher(distribution));
        }
    }

    public void addDistributionToDistributionCampaign(@NonNull DistributionCampaign distributionCampaign,
                                                      @NonNull long id,
                                                      @NonNull long amount,
                                                      @NonNull LocalDate startDate,
                                                      @NonNull User user){
        addDistributionToDistributionCampaign(distributionCampaign, id, amount, startDate, user, VoucherType.GIFT);
    }

    public void addDistributionToDistributionCampaign(@NonNull DistributionCampaign distributionCampaign,
                                                      @NonNull long id,
                                                      @NonNull long amount,
                                                      @NonNull LocalDate startDate,
                                                      @NonNull User user,
                                                      @NonNull VoucherType voucherType){
        distributionCampaign.getDistributionList().add(voucherType.equals(VoucherType.GIFT)
                ? distributionService.createGiftVoucherDistribution(id, amount, startDate, distributionCampaign.getCompany(), user)
                : distributionService.createMealVoucherDistribution(id, amount, startDate, distributionCampaign.getCompany(), user));
    }

    public DistributionCampaign create(@NonNull final DistributionCampaign distributionCampaign){
        return distributionCampaignRepository.create(distributionCampaign);
    }

    public Optional<DistributionCampaign> findById(@NonNull final long distributionCampaignId){
        return Optional.ofNullable(distributionCampaignRepository.findById(distributionCampaignId));
    }
}
