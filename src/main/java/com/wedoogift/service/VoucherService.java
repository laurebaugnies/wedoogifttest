package com.wedoogift.service;

import com.wedoogift.entity.VoucherType;
import com.wedoogift.entity.GiftVoucher;
import com.wedoogift.entity.MealVoucher;
import com.wedoogift.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class VoucherService {

    @Autowired
    private UserService userService;

    public void addVoucherToUser(@NonNull User user, @NonNull long amount, @NonNull int voucherTypeId){
        userService.addVoucherToUser(user, VoucherType.GIFT.getId() == voucherTypeId
                ? createGiftVoucher(amount)
                : createMealVoucher(amount));
    }

    public GiftVoucher createGiftVoucher(@NonNull long amount){
        return createGiftVoucher(amount, LocalDate.now());
    }

    public GiftVoucher createGiftVoucher(@NonNull long amount, @NonNull LocalDate startDate){
        return new GiftVoucher(amount, startDate);
    }

    public MealVoucher createMealVoucher(@NonNull long amount){
        return createMealVoucher(amount, LocalDate.now());
    }

    public MealVoucher createMealVoucher(@NonNull long amount, @NonNull LocalDate startDate){
        return new MealVoucher(amount, startDate);
    }
}
