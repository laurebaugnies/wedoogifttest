package com.wedoogift.service;

import com.wedoogift.entity.Account;
import com.wedoogift.entity.Voucher;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    public void addVoucherToAccount(@NonNull Account account, @NonNull Voucher voucher){
        account.getVoucherList().add(voucher);
    }
}
