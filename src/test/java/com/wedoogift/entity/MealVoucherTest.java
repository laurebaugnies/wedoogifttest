package com.wedoogift.entity;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class MealVoucherTest {

    @Test
    public void defaultConstructorTest(){
        //given when
        MealVoucher mealVoucher = new MealVoucher(10, LocalDate.now());
        //then
        assertEquals(10, mealVoucher.getAmount());
        assertEquals(LocalDate.of(2022, 2, 28), mealVoucher.getEndDate());
    }

    @Test
    public void isCurrentlyValidTest(){
        //given when
        MealVoucher mealVoucher = new MealVoucher(10, LocalDate.now());
        //then
        assertTrue(mealVoucher.isCurrentlyValid());
    }

    @Test
    public void isCurrentlyValidFalseTest(){
        //given when
        MealVoucher mealVoucher = new MealVoucher(10, LocalDate.of(2020, 1, 1));
        //then
        assertFalse(mealVoucher.isCurrentlyValid());
    }
}