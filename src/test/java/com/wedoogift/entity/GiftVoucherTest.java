package com.wedoogift.entity;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class GiftVoucherTest {

    @Test
    public void defaultConstructorTest(){
        //given when
        GiftVoucher giftVoucher = new GiftVoucher(10, LocalDate.now());
        //then
        assertEquals(10, giftVoucher.getAmount());
        assertEquals(LocalDate.now().plusDays(364), giftVoucher.getEndDate());
    }

    @Test
    public void calculateEndDateOnConstructTest(){
        //given when
        GiftVoucher giftVoucher = new GiftVoucher(10, LocalDate.now().minusDays(364));
        //then
        assertEquals(10, giftVoucher.getAmount());
        assertEquals(LocalDate.now(), giftVoucher.getEndDate());
    }

    @Test
    public void isCurrentlyValidTest(){
        //given when
        GiftVoucher giftVoucher = new GiftVoucher(10, LocalDate.now());
        //then
        assertTrue(giftVoucher.isCurrentlyValid());
    }

    @Test
    public void isCurrentlyValidFalseTest(){
        //given when
        GiftVoucher giftVoucher = new GiftVoucher(10, LocalDate.now().minusDays(365));
        //then
        assertFalse(giftVoucher.isCurrentlyValid());
    }
}