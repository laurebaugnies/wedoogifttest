package com.wedoogift.entity;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserTest {

    @Test
    public void getUserBalanceTest(){
        //given
        Account account = new Account();
        GiftVoucher giftVoucher1 = new GiftVoucher(1, LocalDate.now());
        GiftVoucher giftVoucher2 = new GiftVoucher(10, LocalDate.now().minusDays(363));
        GiftVoucher giftVoucher3 = new GiftVoucher(100, LocalDate.now().minusDays(364));
        account.getVoucherList().addAll(Arrays.asList(giftVoucher1, giftVoucher2, giftVoucher3));
        User user = new User();
        user.setAccount(account);
        //when
        long accountBalance = user.getAccount().getBalance();
        //then
        assertEquals(11, accountBalance);
    }

    @Test
    public void getAccountBalanceWithEmptyGiftListTest(){
        //given
        User user = new User();
        //when
        long accountBalance = user.getAccount().getBalance();
        //then
        assertEquals(0, accountBalance);
    }
}
