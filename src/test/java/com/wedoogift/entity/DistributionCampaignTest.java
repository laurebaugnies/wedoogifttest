package com.wedoogift.entity;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class DistributionCampaignTest {

    @Test
    public void isBalanceSufficientTest(){
        //given
        DistributionCampaign distributionCampaign = new DistributionCampaign();
        Company company = new Company();
        company.setBalance(30);
        distributionCampaign.setCompany(company);
        distributionCampaign.getDistributionList().add(createDistribution());
        distributionCampaign.getDistributionList().add(createDistribution());
        distributionCampaign.getDistributionList().add(createDistribution());
        //when
        boolean isBalanceSufficient = distributionCampaign.isBalanceSufficient();
        //then
        assertTrue(isBalanceSufficient);
    }

    @Test
    public void isBalanceSufficientFalseTest(){
        //given
        DistributionCampaign distributionCampaign = new DistributionCampaign();
        Company company = new Company();
        company.setBalance(20);
        distributionCampaign.setCompany(company);
        distributionCampaign.getDistributionList().add(createDistribution());
        distributionCampaign.getDistributionList().add(createDistribution());
        distributionCampaign.getDistributionList().add(createDistribution());
        //when
        boolean isBalanceSufficient = distributionCampaign.isBalanceSufficient();
        //then
        assertFalse(isBalanceSufficient);
    }

    private Distribution createDistribution(){
        Distribution distribution = new Distribution();
        distribution.setVoucher(new GiftVoucher(10, LocalDate.now()));
        distribution.setUser(new User());
        return distribution;
    }
}