package com.wedoogift.service;

import com.wedoogift.entity.GiftVoucher;
import com.wedoogift.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void addVoucherToUserTest(){
        //given
        User user = new User();
        GiftVoucher giftVoucher = new GiftVoucher(10, LocalDate.now());
        //when
        userService.addVoucherToUser(user, giftVoucher);
        //then
        assertEquals(1, user.getAccount().getVoucherList().size());
        assertTrue(user.getAccount().getVoucherList().contains(giftVoucher));
    }
}