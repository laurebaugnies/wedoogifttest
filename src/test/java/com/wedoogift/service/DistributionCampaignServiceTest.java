package com.wedoogift.service;

import com.wedoogift.entity.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class DistributionCampaignServiceTest {


    @Autowired
    private DistributionCampaignService distributionCampaignService;

    @Test
    public void distributeVoucherTest(){
        //given
        DistributionCampaign distributionCampaign = new DistributionCampaign();
        Company company = new Company();
        company.setBalance(30);
        User user1 = new User();
        User user2 = new User();
        User user3 = new User();
        distributionCampaign.setCompany(company);
        distributionCampaign.getDistributionList().add(createDistribution(user1, company));
        distributionCampaign.getDistributionList().add(createDistribution(user2, company));
        distributionCampaign.getDistributionList().add(createDistribution(user3, company));
        //when
        distributionCampaignService.distributeVoucher(distributionCampaign);
        //then
        assertEquals(10, user1.getAccount().getBalance());
        assertEquals(10, user2.getAccount().getBalance());
        assertEquals(10, user3.getAccount().getBalance());
        assertEquals(0, company.getBalance());
    }


    @Test
    public void distributeVoucherWithInsufficientBalanceTest(){
        //given
        DistributionCampaign distributionCampaign = new DistributionCampaign();
        Company company = new Company();
        company.setBalance(10);
        User user1 = new User();
        User user2 = new User();
        User user3 = new User();
        distributionCampaign.setCompany(company);
        distributionCampaign.getDistributionList().add(createDistribution(user1, company));
        distributionCampaign.getDistributionList().add(createDistribution(user2, company));
        distributionCampaign.getDistributionList().add(createDistribution(user3, company));
        //when
        distributionCampaignService.distributeVoucher(distributionCampaign);
        //then
        assertEquals(0, user1.getAccount().getBalance());
        assertEquals(0, user2.getAccount().getBalance());
        assertEquals(0, user3.getAccount().getBalance());
        assertEquals(10, company.getBalance());
    }

    private Distribution createDistribution(User user, Company company){
        Distribution distribution = new Distribution();
        distribution.setVoucher(new GiftVoucher(10, LocalDate.now()));
        distribution.setUser(user);
        distribution.setCompany(company);
        return distribution;
    }
}