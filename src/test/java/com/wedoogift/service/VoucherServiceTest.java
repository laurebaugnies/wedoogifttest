package com.wedoogift.service;

import com.wedoogift.entity.GiftVoucher;
import com.wedoogift.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class VoucherServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private VoucherService voucherService;

    @Test
    public void addGiftVoucherToUser(){
        //given
        User user = new User();
        GiftVoucher giftVoucher = voucherService.createGiftVoucher(10, LocalDate.now());
        //when
        userService.addVoucherToUser(user, giftVoucher);
        //then
        assertTrue(user.getAccount().getVoucherList().contains(giftVoucher));
        assertEquals(10, user.getAccount().getBalance());
    }
}