package com.wedoogift.service;

import com.wedoogift.entity.Account;
import com.wedoogift.entity.GiftVoucher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @Test
    public void addGiftVoucherToAccountTest(){
        //given
        Account account = new Account();
        GiftVoucher giftVoucher = new GiftVoucher(10, LocalDate.now());
        //when
        accountService.addVoucherToAccount(account, giftVoucher);
        //then
        assertEquals(1, account.getVoucherList().size());
        assertTrue(account.getVoucherList().contains(giftVoucher));
    }
}