package com.wedoogift.controller;

import com.wedoogift.entity.DistributionCampaign;
import com.wedoogift.service.DistributionCampaignService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class DistributionCampaignControllerTest extends ControllerTest{

    @Autowired
    private DistributionCampaignService distributionCampaignService;

    @Test
    public void testWithoutLogin() throws Exception {
        mockMvc.perform(get("/distributeVoucher?distributionCampaignId=1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andReturn();
    }

    @Test
    public void testWithWrongCredentials() throws Exception {
        mockMvc.perform(get("/distributeVoucher?distributionCampaignId=1")
                .with(user("user").password("00000").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn();
    }


    @Test
    public void test() throws Exception {
        mockMvc.perform(get("/distributeVoucher?distributionCampaignId=1")
                .with(user("user").password("123456").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void testWithDistributionCampaign() throws Exception{
        DistributionCampaign distributionCampaign = new DistributionCampaign();
        distributionCampaignService.create(distributionCampaign);
        MvcResult result = mockMvc.perform(get("/distributeVoucher?distributionCampaignId=1")
                .with(user("admin").password("123456").roles("USER", "ADMIN")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals("true", result.getResponse().getContentAsString());
    }

    @Test
    public void testForAdminUser() throws Exception {
        MvcResult result = mockMvc.perform(get("/distributeVoucher?distributionCampaignId=1")
                .with(user("admin").password("123456").roles("USER", "ADMIN")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals("false", result.getResponse().getContentAsString());
    }


    @Test
    public void testWithoutCampaignId() throws Exception{
        mockMvc.perform(get("/distributeVoucher?")
                .with(user("user").password("123456").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }
}