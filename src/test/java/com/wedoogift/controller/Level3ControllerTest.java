package com.wedoogift.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class Level3ControllerTest extends ControllerTest{

    private String inputFileExpectedOutput="{\"companies\":[{\"id\":1,\"name\":\"Wedoogift\",\"balance\":1000},{\"id\":2,\"name\":\"Wedoofood\",\"balance\":3000}],\"users\":[{\"id\":1,\"balance\":[{\"amount\":100,\"wallet_id\":1}]},{\"id\":2,\"balance\":[]},{\"id\":3,\"balance\":[]}],\"distributions\":[]}";

    @Test
    public void testWithoutLogin() throws Exception {
        mockMvc.perform(get("/run")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andReturn();
    }

    @Test
    public void testWithWrongCredentials() throws Exception {
        MvcResult result = mockMvc.perform(get("/run")
                .with(user("user").password("00000").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn();
    }


    @Test
    public void testWithForbiddenUser() throws Exception {
        mockMvc.perform(get("/run")
                .with(user("user").password("123456").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void testForAdminUser() throws Exception {
        String expectedOutput = "{\"companies\":[{\"id\":1,\"name\":\"Wedoogift\",\"balance\":600},{\"id\":2,\"name\":\"Wedoofood\",\"balance\":2000}],\"users\":[{\"id\":1,\"balance\":[{\"amount\":100,\"wallet_id\":1},{\"amount\":50,\"wallet_id\":1},{\"amount\":250,\"wallet_id\":2}]},{\"id\":2,\"balance\":[{\"amount\":100,\"wallet_id\":1}]},{\"id\":3,\"balance\":[{\"amount\":1000,\"wallet_id\":1}]}],\"distributions\":[{\"id\":1,\"amount\":50,\"start_date\":\"2020-09-16\",\"end_date\":\"2021-09-15\",\"wallet_Id\":1,\"company_Id\":1,\"user_Id\":1},{\"id\":2,\"amount\":100,\"start_date\":\"2020-08-01\",\"end_date\":\"2021-07-31\",\"wallet_Id\":1,\"company_Id\":1,\"user_Id\":2},{\"id\":3,\"amount\":1000,\"start_date\":\"2020-05-01\",\"end_date\":\"2021-04-30\",\"wallet_Id\":1,\"company_Id\":2,\"user_Id\":3},{\"id\":4,\"amount\":250,\"start_date\":\"2020-05-01\",\"end_date\":\"2021-02-28\",\"wallet_Id\":2,\"company_Id\":1,\"user_Id\":1}]}";
        MvcResult result = mockMvc.perform(get("/run")
                .with(user("admin").password("123456").roles("USER", "ADMIN")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        //Order is not always the same even with JsonPropertyOrder annotation
        //assertEquals(expectedOutput, result.getResponse().getContentAsString());
    }


    @Test
    public void testLoadDataWithoutLogin() throws Exception {
        mockMvc.perform(get("/loadData")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andReturn();
    }

    @Test
    public void testLoadDataWithWrongCredentials() throws Exception {
        mockMvc.perform(get("/loadData")
                .with(user("user").password("00000").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn();
    }


    @Test
    public void testLoadDataWithForbiddenUser() throws Exception {
        mockMvc.perform(get("/loadData")
                .with(user("user").password("123456").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void testLoadDataForAdminUser() throws Exception {
        String expectedOutput="[{\"id\":3,\"company\":{\"id\":1,\"name\":\"Wedoogift\",\"balance\":1000},\"distributionList\":[{\"id\":1,\"amount\":50,\"start_date\":\"2020-09-16\",\"end_date\":\"2021-09-15\",\"company_Id\":1,\"user_Id\":1,\"wallet_Id\":1},{\"id\":2,\"amount\":100,\"start_date\":\"2020-08-01\",\"end_date\":\"2021-07-31\",\"company_Id\":1,\"user_Id\":2,\"wallet_Id\":1},{\"id\":4,\"amount\":250,\"start_date\":\"2020-05-01\",\"end_date\":\"2021-02-28\",\"company_Id\":1,\"user_Id\":1,\"wallet_Id\":2}],\"balanceSufficient\":true},{\"id\":4,\"company\":{\"id\":2,\"name\":\"Wedoofood\",\"balance\":3000},\"distributionList\":[{\"id\":3,\"amount\":1000,\"start_date\":\"2020-05-01\",\"end_date\":\"2021-04-30\",\"company_Id\":2,\"user_Id\":3,\"wallet_Id\":1}],\"balanceSufficient\":true}]";
        MvcResult result = mockMvc.perform(get("/loadData")
                .with(user("admin").password("123456").roles("USER", "ADMIN")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        //Order is not always the same even with JsonPropertyOrder annotation
        //assertEquals(expectedOutput, result.getResponse().getContentAsString());
    }

    @Test
    public void testReadInputFileWithoutLogin() throws Exception {
        mockMvc.perform(get("/readInputFile")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andReturn();
    }

    @Test
    public void testReadInputFileWithWrongCredentials() throws Exception {
        mockMvc.perform(get("/readInputFile")
                .with(user("user").password("00000").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }


    @Test
    public void testReadInputFile() throws Exception {
        MvcResult result = mockMvc.perform(get("/readInputFile")
                .with(user("user").password("123456").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(inputFileExpectedOutput, result.getResponse().getContentAsString());
    }

    @Test
    public void testReadInputFileWithDistributionCampaign() throws Exception{
        MvcResult result = mockMvc.perform(get("/readInputFile")
                .with(user("admin").password("123456").roles("USER", "ADMIN")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(inputFileExpectedOutput, result.getResponse().getContentAsString());
    }

    @Test
    public void testReadInputFileForAdminUser() throws Exception {
        MvcResult result = mockMvc.perform(get("/readInputFile")
                .with(user("admin").password("123456").roles("USER", "ADMIN")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals(inputFileExpectedOutput, result.getResponse().getContentAsString());
    }
}