package com.wedoogift.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.*;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class UserControllerTest extends ControllerTest{


    @Test
    public void testWithoutLogin() throws Exception {
        mockMvc.perform(get("/getBalance?userId=1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andReturn();
    }

    @Test
    public void test() throws Exception {
        MvcResult result = mockMvc.perform(get("/getBalance?userId=1")
                .with(user("user").password("123456").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals("0", result.getResponse().getContentAsString());
    }


    @Test
    public void testForAdminUser() throws Exception {
        MvcResult result = mockMvc.perform(get("/getBalance?userId=1")
                .with(user("admin").password("123456").roles("USER", "ADMIN")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals("0", result.getResponse().getContentAsString());
    }


    @Test
    public void testWithoutUserId() throws Exception{
        mockMvc.perform(get("/getBalance")
                .with(user("user").password("123456").roles("USER")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }
}